FROM node:8 as builder
WORKDIR /usr/src/app
COPY ./ ./
RUN npm install && npm run build


FROM nginx:1.17
COPY --from=builder /usr/src/app/dist /usr/share/nginx/html/
COPY ./default.conf /etc/nginx/conf.d/default.conf
EXPOSE 80