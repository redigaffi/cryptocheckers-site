import Home from '../components/Home.vue';
import Login from '../components/Login.vue';
import Register from '../components/Register.vue';
import Profile from '../components/Profile.vue';
import Deposit from '../components/Deposit.vue';
import Transactions from '../components/Transactions.vue';
import Logout from '../components/Logout.vue';
import GameClient from '../components/GameClient.vue';
import Cashout from '../components/Cashout.vue';
import ConfirmEmail from '../components/ConfirmEmail.vue';

const routes = [
    { 
        path: '/',
        component: Home,
        name: 'index',
        meta: {requiresAuth: false}
    },
    { 
        path: '/login',
        component: Login,
        name: 'login',
        meta: {requiresAuth: false}
    },
    { 
        path: '/logout',
        component: Logout,
        name: 'logout',
        meta: {requiresAuth: true}
    },
    { 
        path: '/register/:referredBy?',
        component: Register,
        name: 'register' ,
        meta: {requiresAuth: false}
    },
    { 
        path: '/profile',
        component: Profile,
        name: 'profile',
        meta: {requiresAuth: true}
        
    },
    { 
        path: '/deposit',
        component: Deposit,
        name: 'deposit',
        meta: {requiresAuth: true}
        
    },
    { 
        path: '/transactions',
        component: Transactions,
        name: 'transactions',
        meta: {requiresAuth: true}
        
    },
    { 
        path: '/client',
        component: GameClient,
        name: 'gameclient',
        meta: {requiresAuth: true}
        
    },
    { 
        path: '/cashout',
        component: Cashout,
        name: 'cashout',
        meta: {requiresAuth: true}
        
    },
    { 
        path: '/confirm/email/:hash',
        component: ConfirmEmail,
        name: 'confirm-email',
        meta: {requiresAuth: false}
        
    },
];

export default routes;