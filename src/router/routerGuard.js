import store from '../store/store.js'
export default function routerGuard(to, from, next) {
   
    if (to.matched.some(record => record.meta.requiresAuth)) {
          
        const session = store.state.loggedIn;
    
        if (!session) {
            next({
                name: "login",
                query: { redirect: to.fullPath }
            });
        } else {
            next();
        }
    
    } else if (to.matched.some(record => !record.meta.requiresAuth)) {
        const session = store.state.loggedIn;
        
        if (session) {
            next({
                name: "profile",
                query: { redirect: to.fullPath }
            });
        } else {
            next();
        }
    } else {
        next();
       
    }
}