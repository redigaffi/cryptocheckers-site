import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import routes from './router/routes.js';
import routerGuard from './router/routerGuard.js';
import Vuesax from 'vuesax';
import axios from 'axios';
import VueAxios from 'vue-axios';
import 'material-icons/iconfont/material-icons.css';
import 'vuesax/dist/vuesax.css';
import 'bootstrap'; 
import 'bootstrap/dist/css/bootstrap.min.css'
import 'vue-flash-message/dist/vue-flash-message.min.css'
import './assets/css/custom.css';
import store from './store/store.js';
import interceptorsSetup from './axios_interceptors/interceptors'
import VueFlashMessage from 'vue-flash-message';
import VueSocketIO from 'vue-socket.io'

// Font Awesome
import { library } from '@fortawesome/fontawesome-svg-core'
import { faHeart, faHeartBroken} from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

library.add(faHeart, faHeartBroken);
Vue.component('font-awesome-icon', FontAwesomeIcon)
// End Font Awesome

Vue.use(new VueSocketIO({
  debug: false,
  connection: process.env.VUE_APP_GAME_SERVER_URL
}));

Vue.config.productionTip = false

store.subscribe((mutation, state) => {
	localStorage.setItem('store', JSON.stringify(state));
});
store.commit('initialiseStore');

const router = new VueRouter({
  mode: 'history',
  routes: routes});
router.beforeEach(routerGuard);

Vue.use(Vuesax);
Vue.use(VueAxios, axios);
Vue.use(VueRouter);
Vue.use(VueFlashMessage);


interceptorsSetup();

export const VueInstance = new Vue({
  store,
  router,
  render: h => h(App),
  
}).$mount('#app');


