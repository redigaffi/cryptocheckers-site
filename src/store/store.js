import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
   state: {
        
       loggedIn: false,
       authenticationToken: false,
        
   },
   mutations: {
       initialiseStore(state) {
              // Check if the ID exists
              if(localStorage.getItem('store')) {
                     this.replaceState(
                            Object.assign(state, JSON.parse(localStorage.getItem('store')))
                     );
              }
       },
       login(state, payload) {    
               state.authenticationToken = payload.authenticationToken;
               state.loggedIn = true;
        },
        destroySession(state) {
               localStorage.removeItem('store');
               state.authenticationToken = false;
               state.loggedIn = false;
        },
   },
  
   getters: {
     loggedIn: state => state.loggedIn,
     authenticationToken: state => state.authenticationToken
   },
   
});

//https://stackoverflow.com/questions/47946680/axios-interceptor-in-vue-2-js-using-vuex