function Cell (y, x)  {
    this.movable;
    this.piece;
    this.y = y;
    this.x = x;
    this.playerFirstRow;
}

Cell.prototype.setIsMovable = function(isMovable) {
    this.movable = isMovable;
};

Cell.prototype.isMovable = function() {
    return this.movable;
};

Cell.prototype.setPiece = function(piece) {
    this.piece = piece;
};

Cell.prototype.getPiece = function() {
    return this.piece;
};

Cell.prototype.getFormattedId = function() {
    return this.y+"-"+this.x;
};

Cell.prototype.hasPiece = function() {
    return this.piece !== undefined;
};

Cell.prototype.removePiece = function() {
    this.piece = undefined;
};

Cell.prototype.getCellElement = function() {
    return document.getElementById(this.getFormattedId());
};

Cell.prototype.getPlayerFirstRow = function() {
    return this.playerFirstRow;
};

Cell.prototype.setPlayerFirstRow = function(playerFirstRow) {
    this.playerFirstRow = playerFirstRow;
};

Cell.prototype.getCellY = function() {
    return this.y;
};

Cell.prototype.getCellX = function() {
    return this.x;
};

export default Cell;