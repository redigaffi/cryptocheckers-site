import Cell from './Cell';
import Piece from './Piece';

function Board(player, boardInfo, socket, turnDomElement, audio) {
    this.player = player;
    this.socket = socket;
    this.yOffset = 10;
    this.xOffset = 10;
    this.turnDomElement = turnDomElement;
    this.audio = audio;
    this.board = [
        ["", "", "", "", "", "", "", "", "", ""],
        ["", "", "", "", "", "", "", "", "", ""],
        ["", "", "", "", "", "", "", "", "", ""],
        ["", "", "", "", "", "", "", "", "", ""],
        ["", "", "", "", "", "", "", "", "", ""],
        ["", "", "", "", "", "", "", "", "", ""],
        ["", "", "", "", "", "", "", "", "", ""],
        ["", "", "", "", "", "", "", "", "", ""],
        ["", "", "", "", "", "", "", "", "", ""],
        ["", "", "", "", "", "", "", "", "", ""]
    ];

    this.parseBoard(boardInfo.board);
    this.playerTurn = boardInfo.playerTurn;
    this.player1 = boardInfo.players[0];
    this.player2 = boardInfo.players[1];
    this.setFrontendTurn();
}
Board.prototype.setFrontendTurn = function() {
    let turnInfoDivs = this.turnDomElement.getElementsByTagName("div");
    let myTurnDiv = turnInfoDivs[0];
    let countDownDiv = turnInfoDivs[1];

    if (this.player.isMyTurn(this.playerTurn)) {
        myTurnDiv.classList.add("my_turn");
        myTurnDiv.classList.remove("waiting_turn");
        countDownDiv.style.display = "";
        myTurnDiv.innerHTML = "MY TURN";

    } else {
        myTurnDiv.classList.add("waiting_turn");
        myTurnDiv.classList.remove("my_turn");
        countDownDiv.style.display = "none";
        myTurnDiv.innerHTML = "WAITING OPPONENT";
    }

};

Board.prototype.parseBoard = function(jsonBoard) {
    for (let y = 0; y < jsonBoard.length; y++) {
        let row = jsonBoard[y];

        for (let x = 0; x < row.length; x++) {
            let column = row[x];
            let cell = new Cell(column.y, column.x);
            cell.setIsMovable(column.movable);

            if (column.playerFirstRow !== "") {
                cell.setPlayerFirstRow(column.playerFirstRow);
            }

            if (column.piece !== false) {
                let piece = new Piece(column.piece.player, column.piece.id, column.piece.y, column.piece.x);

                if (column.piece.isKing) {
                    piece.convertToKing();
                }

                if (this.player.isMyPiece(piece)) {
                    this.player.addPiece(piece);
                }

                cell.setPiece(piece);
            }
            this.board[y][x] = cell;
        }
    }
};

Board.prototype.printStartingBoard = function() {
    let kingPiecesToDraw = [];
    let table = document.createElement("table");

    for (let y = 0; y < this.yOffset; y++) {
        let tr = table.insertRow();
        
        for(let x = 0; x < this.xOffset; x++) {
            let td = tr.insertCell();
            let currentCell = this.getCellByPosition(y, x);
            
            if (currentCell.isMovable()) {
                if (currentCell.hasPiece()) {
                    let currentPiece = currentCell.getPiece();
                    let span = document.createElement("span");
                    span.setAttribute("id", currentPiece.getFormattedId());
                    span.classList.add("dot");
                    span.classList.add(currentPiece.getPlayer());

                    if (this.player.isMyPiece(currentPiece)) {
                        span.addEventListener("click", this.player.highlightPieceHandler.bind(this.player));
                    }

                    td.appendChild(span);

                    if (currentPiece.getIsKing()) {
                        span.classList.add(currentPiece.getPlayer() + "_king");
                    }
                }

                td.classList.add("movable");
                td.setAttribute("id", currentCell.getFormattedId());
                td.addEventListener("click", this.movePieceHandler.bind(this));
            }
        }
    }

    return table;
};

/**
 * @param y
 * @param x
 * @returns Cell|Boolean
 */
Board.prototype.getCellByPosition = function(y, x) {
    if (this.board[y] === undefined) {
        return false;
    }

    let cell = this.board[y][x];

    if (cell === undefined) {
        return false;
    }

    return cell;
};

Board.prototype.convertPieceToKingIfOpponentRowReached = function(toCell, piece)
{
    if (this.player.isMyPiece(piece) && toCell.getPlayerFirstRow() === this.getOpponentByPlayer(this.player.getId())) {
        piece.convertToKing();
        piece.getPieceElement().classList.add(piece.getPlayer() + "_king");
        return true;
    }

    return false;
};

Board.prototype.getPiecesThatCanEat = function() {
    let myPieces = this.player.getPieces();
    let piecesThatCanEat = [];

    for (let index in myPieces) {
        let myPiece = myPieces[index];

        let validMovementsForPiece = myPiece.getValidMovements(this);
        if (validMovementsForPiece.getCanAnyMovementEat()) {
            piecesThatCanEat.push(myPiece);
        }
    }

    return piecesThatCanEat;
};


Board.prototype.checkIfPieceCanEat = function(pieceToMove) {
    let piecesThatCanEat = this.getPiecesThatCanEat();

    let pieceFound = true;
    if (piecesThatCanEat.length > 0) {
        pieceFound = false;

        for (let i = 0; i < piecesThatCanEat.length; i++) {
            let pieceThatCanEat = piecesThatCanEat[i];
            if (pieceToMove.getId() === pieceThatCanEat.getId()) {
                pieceFound = true;
                break;
            }
        }
    }

    return pieceFound;
};


Board.prototype.movePieceHandler = function(clickedCell) {
    if (this.player.getLastHighlightedPiece() === undefined || !this.player.isMyTurn(this.playerTurn)) {
        console.log("if1");
        return;
    }

    let fromCellId = this.player.getLastHighlightedPiece().parentNode.id;
    let [fromCellY, fromCellX] = fromCellId.split("-");
    let fromCell = this.getCellByPosition(fromCellY, fromCellX);
    let pieceToMove = fromCell.getPiece();

    let canMove = this.checkIfPieceCanEat(pieceToMove);
    if (!canMove) {
        console.log("if2");
        return;
    }

    let toCellPositionId = clickedCell.currentTarget.id;
    let [toCellY, toCellX] = toCellPositionId.split("-");
    toCellY = parseInt(toCellY);
    toCellX = parseInt(toCellX);
    let toCell = this.getCellByPosition(toCellY, toCellX);

    let validMovementsForPieceCollection = pieceToMove.getValidMovements(this);
    let validMovementsForPiece = validMovementsForPieceCollection.asArray();

    // if there's any piece that can eat that should be the only valid movement.
    let validMovement = this.player.isMovementValid(validMovementsForPiece, toCellY, toCellX);
    if (validMovement !== false) {

        let movePieceEventData = {pieceToMove: Object.assign({}, pieceToMove), toCell: Object.assign({}, toCell), changeTurn: false};
        this.movePiece(fromCell, toCell);
        let pieceConvertedToKing = this.convertPieceToKingIfOpponentRowReached(toCell, pieceToMove);
        if (pieceConvertedToKing) {
            movePieceEventData.convertPieceToKing = pieceConvertedToKing;
        }

        let hasEaten = false;
        if (validMovement.getCanEatPiece()) {
            let cellToEat = validMovement.getCellToEat();
            let pieceToRemove = cellToEat.getPiece();
            this.player.addEatenPiece(pieceToRemove);
            movePieceEventData.cellToEat = Object.assign({}, cellToEat);
            this.removePiece(cellToEat);
            hasEaten = true;
        } else {
            this.player.resetLastClickedPieceElement();
        }

        // After playing a while this seems to get buggy, it doesn't changes the turn correctly.
        let canStillEatValidMovementsCollection = pieceToMove.getValidMovements(this);
        if ( !(hasEaten && canStillEatValidMovementsCollection.getCanAnyMovementEat()) || pieceConvertedToKing) {
            movePieceEventData.changeTurn = true;
            this.changeTurn();
            this.player.resetLastClickedPieceElement();
        }

        this.socket.emit('move_piece_event', movePieceEventData);
    }
};

Board.prototype.removePiece = function(cellToEat) {
    let pieceToRemove = cellToEat.getPiece();
    pieceToRemove.getPieceElement().remove();
    cellToEat.removePiece();
};

Board.prototype.movePiece = function(fromCell, toCell) {
    let pieceToMove = fromCell.getPiece();
    pieceToMove.updatePosition(toCell.getCellY(), toCell.getCellX());
    toCell.setPiece(pieceToMove);
    fromCell.removePiece();
    let pieceToMoveDom = pieceToMove.getPieceElement();
    pieceToMoveDom.remove();
    toCell.getCellElement().appendChild(pieceToMoveDom);
    
};

Board.prototype.getOpponentByPlayer = function(player) {
    if (player === this.player1) {
        return this.player2;
    } else if (player === this.player2) {
        return this.player1;
    }
};

Board.prototype.changeTurn = function() {
    if (this.playerTurn === this.player1) {            
        this.playerTurn = this.player2;
    } else if (this.playerTurn === this.player2) {        
        this.playerTurn = this.player1;
    }

    if (this.player.isMyTurn(this.playerTurn)) {
        this.audio.play();
    }

    this.setFrontendTurn();
};

export default Board;