function Player(id, name, socket) {
    this.id = id;
    this.name = name;
    this.lastClickedPieceElement;
    this.pieces = [];
    this.eatenPieces = [];
    this.strikes = 0;
    this.socket = socket;
}


Player.prototype.getId = function() {
    return this.id;
};

Player.prototype.getStrikes = function() {
    return this.strikes;
};

Player.prototype.setStrikes = function(strikes) {
    this.strikes = strikes;
};

Player.prototype.isMyPiece = function(piece) {
    return this.id === piece.getPlayer();
};

Player.prototype.highlightPieceHandler = function(pieceEvent) {

    let pieceElement = pieceEvent.target;
    
    if (this.lastClickedPieceElement !== undefined) {
        this.lastClickedPieceElement.classList.remove('pieceSelected');
    } 

    this.lastClickedPieceElement = pieceElement;
    this.lastClickedPieceElement.classList.add('pieceSelected');

    pieceEvent.stopPropagation();
};

Player.prototype.getLastHighlightedPiece = function() {
    return this.lastClickedPieceElement;
};

Player.prototype.resetLastClickedPieceElement = function() {
    if (this.lastClickedPieceElement !== undefined) {
        this.lastClickedPieceElement.classList.remove('pieceSelected');
        this.lastClickedPieceElement = undefined;
    }
};

Player.prototype.isMyTurn = function(playersTurn) {
    return playersTurn === this.id;    
};

Player.prototype.addPiece = function(piece) {
    this.pieces[piece.getId()] = piece;
};

Player.prototype.removePiece = function(piece) {
    delete this.pieces[piece.getId()];
};

Player.prototype.getPieces = function() {
    return this.pieces;
};

Player.prototype.addEatenPiece = function(eatenPiece) {
    this.eatenPieces.push(eatenPiece);
};

Player.prototype.getMyName = function() {
    return this.name;
};

Player.prototype.isMovementValid = function(validMovements, y, x) {
    let canMove = false;
    for (let i = 0; i < validMovements.length; i++) {
        let validMovement = validMovements[i];
        let vY = validMovement.getY();
        let vX = validMovement.getX();

        canMove = (vY === y && vX === x);

        if (canMove) {
            return validMovement;
        }
    }

    return canMove;
};


export default Player;