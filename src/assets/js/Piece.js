import ValidMovement from './ValidMovement';
import ValidMovementCollection from './ValidMovementCollection';

function Piece(player, id, y, x) {
    this.player = player;
    this.id = id;
    this.y = y;
    this.x = x;
    this.isKing = false;
}

Piece.prototype.convertToKing = function() {
    this.isKing = true;
    if (this.getPieceElement() !== null) {
        this.getPieceElement().classList.add(this.getPlayer() + "_king");
    }
};

Piece.prototype.getId = function() {
    return this.id;
};

Piece.prototype.getPlayer = function() {
    return this.player;
};

Piece.prototype.getFormattedId = function() {
    return this.getPlayer() + "-p"+this.getId();
};

Piece.prototype.getPieceElement = function() {
    return document.getElementById(this.getFormattedId());
};

Piece.prototype.updatePosition = function(y, x) {
    this.y = y;
    this.x = x;
};

Piece.prototype.getValidMovements = function(board) {
    if (this.isKing) {
        return this.generateValidMovementsForKingPiece(board);
    } else {
        return this.generateValidMovementsForNormalPiece(board);
    }
};

Piece.prototype.generateValidMovementsForKingPiece = function(board) {
    let validMovementsFromPosition = [
        this.movementOperations(0, this.y, this.x),
        this.movementOperations(1, this.y, this.x),
        this.movementOperations(2, this.y, this.x),
        this.movementOperations(3, this.y, this.x)
    ];

    let validMovementsFromPositionCopy = validMovementsFromPosition.slice();

    for (let i = 0; i < validMovementsFromPositionCopy.length; i++) {
        let validMovement = validMovementsFromPositionCopy[i];

        let currentY = validMovement.getY();
        let currentX = validMovement.getX();
        let currentOperation = validMovement.getOperationType();
        let diagonalMovementCanEatPiece = false;

        for (let x = 0; x < board.yOffset; x++) {

            let cell = board.getCellByPosition(currentY, currentX);

            if (cell === false) {
                continue;
            }

            if (cell.isMovable() && cell.hasPiece() && cell.getPiece().getPlayer() === this.player) {
                break;
            }

            if (cell.isMovable() && cell.hasPiece() && cell.getPiece().getPlayer() === board.getOpponentByPlayer(this.player)) {
                let nextDiagonalMovement = this.movementOperations(currentOperation, currentY, currentX);
                let nextDiagonalMovementY = nextDiagonalMovement.getY();
                let nextDiagonalMovementX = nextDiagonalMovement.getX();
                let nextDiagonalMovementOperation = nextDiagonalMovement.getOperationType();

                let nextCell = board.getCellByPosition(nextDiagonalMovementY, nextDiagonalMovementX);

                if (nextCell !== false && !nextCell.hasPiece()) {
                    validMovement = new ValidMovement(cell.getPiece().getId(), nextDiagonalMovementOperation, nextDiagonalMovementY, nextDiagonalMovementX, true, cell);
                    diagonalMovementCanEatPiece = cell;

                } else if (nextCell !== false && nextCell.hasPiece()) {
                    break;
                }
            }

            if (diagonalMovementCanEatPiece !== false) {
                validMovement.setCanEatPiece(true);
                validMovement.setCellToEat(diagonalMovementCanEatPiece);
            }

            validMovementsFromPosition.push(validMovement);

            validMovement = this.movementOperations(currentOperation, currentY, currentX);
            currentY = validMovement.getY();
            currentX = validMovement.getX();
            currentOperation = validMovement.getOperationType();
        }
    }

    return new ValidMovementCollection(validMovementsFromPosition);
};

Piece.prototype.generateValidMovementsForNormalPiece = function(board) {

    let validMovementsFromPosition = [
        this.movementOperations(0, this.y, this.x),
        this.movementOperations(1, this.y, this.x),
        this.movementOperations(2, this.y, this.x),
        this.movementOperations(3, this.y, this.x)]
        .filter(this.checkMovementOffset.bind(this, board));

    validMovementsFromPosition = this.checkMovementOccupied(validMovementsFromPosition, board);
    validMovementsFromPosition = this.isEatingMovementPossible(validMovementsFromPosition);
    validMovementsFromPosition = this.removeBackwardMovements(validMovementsFromPosition, board);

    return new ValidMovementCollection(validMovementsFromPosition);
};

Piece.prototype.removeBackwardMovements = function(validMovements, board) {
    let validMovementsResult = [];

    for (let i = 0; i < validMovements.length; i++) {
        let validMovement = validMovements[i];

        // Backward movement allowed when eating
        if (validMovement.getCanEatPiece()) {
            validMovementsResult.push(validMovement);
            continue;
        }

        if (this.player === board.player1) {
            if (validMovement.y > this.y) {
                validMovementsResult.push(validMovement);
            }
        } else if (this.player === board.player2) {
            if (validMovement.y < this.y) {
                validMovementsResult.push(validMovement);
            }
        }
    }

    return validMovementsResult;
};

Piece.prototype.isEatingMovementPossible = function(validMovements) {
    let movementsThatCanEat = [];

    for (let i = 0; i < validMovements.length; i++) {
        let validMovement = validMovements[i];
        if (validMovement.getCanEatPiece()) {
            movementsThatCanEat.push(validMovement);
        }
    }

    if (movementsThatCanEat.length > 0) {
        return movementsThatCanEat
    }

    return validMovements;
};

Piece.prototype.checkMovementOffset = function(board, movement) {
    let y = movement.getY();
    let x = movement.getX();

    return  (y >= 0 && y < board.yOffset) && (x >= 0 && x < board.xOffset);
};

Piece.prototype.checkMovementOccupied = function(validMovements, board) {
    let result = [];

    for (let i = 0; i < validMovements.length; i++) {
        let position = validMovements[i];

        let operationType = position.getOperationType();
        let y = position.getY();
        let x = position.getX();
        let cell = board.getCellByPosition(y, x);

        if (cell.isMovable() && !cell.hasPiece()) {
            result.push(position);
        } else if (cell.isMovable() && cell.hasPiece() && cell.getPiece().getPlayer() === board.getOpponentByPlayer(this.player)) {
            let nextDiagonalMovement = this.movementOperations(operationType, y, x);
            let nxtY = nextDiagonalMovement.getY();
            let nxtX = nextDiagonalMovement.getX();
            let nextCell = board.getCellByPosition(nxtY, nxtX);

            if (nextCell === false) { // reached the offset
                continue;
            }

            if (!nextCell.hasPiece()) {
                result.push(new ValidMovement(this.id, operationType, nxtY, nxtX, true, cell));
            }
        }
    }

    return result;
};

Piece.prototype.movementOperations = function(operation, y, x) {
    let operations = {

        // Up, left
        0: function(y, x) {
            return new ValidMovement(this.id, 0, y-1, x-1, false, false);
        }.bind(this),

        // Up, right
        1: function(y, x) {
            return new ValidMovement(this.id, 1, y-1, x+1, false, false);
        }.bind(this),

        // Down, left
        2: function(y, x) {
            return new ValidMovement(this.id, 2, y+1, x-1, false, false);
        }.bind(this),

        // Down, right
        3: function(y, x) {
            return new ValidMovement(this.id, 3, y+1, x+1, false, false);
        }.bind(this)
    };

    return operations[operation](y, x);
};

Piece.prototype.getIsKing = function(x) {
    return this.isKing;
};

export default Piece;