import axios from 'axios';
import store from '../store/store.js'

export default function setup() {
    axios.interceptors.request.use(function(config) {
        const loggedIn = store.getters.loggedIn;
        const authenticationToken = store.getters.authenticationToken;
        
        if(loggedIn && authenticationToken !== null) {
            config.headers.Authorization = authenticationToken;
        }

        return config;
    }, function(err) {
        console.log(err);
        return Promise.reject(err);
    });

    axios.interceptors.response.use(function (response) {
        
        // Do something with response data
        
        return response;
    }, function (error) {
        

        if (typeof error.message !== "undefined" && error.message === "Request aborted") {
            store.commit("destroySession");
        }

        if (typeof error.message !== "undefined" && error.message === "Network Error") {
            store.commit("destroySession");
        }

        if (typeof error.response !== "undefined") {
            if (error.response.data.exception === "io.jsonwebtoken.SignatureException") {
                store.commit("destroySession");
            }
        }

        return Promise.reject(error);
    });
}