
const baseDomain = process.env.VUE_APP_API_URL;

export default {
    userLogin: `${baseDomain}/login`,
    userProfileInfo: `${baseDomain}/profile`,
    deposit: `${baseDomain}/deposit`,
    notifications: `${baseDomain}/notifications`,
    bettingRooms: `${baseDomain}/bettingRooms`,
    register: `${baseDomain}/register`,
    myTransactions: `${baseDomain}/myTransactions`,
    myBalance: `${baseDomain}/myBalance`,
    confirmEmail: `${baseDomain}/confirm/new-account/`
}